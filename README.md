# README #

### How do I get set up? ###

* Summary of set up
To run the application, Install Visual Studio Community (2015) or above. Open the .sln file and build the solution to run the application. Follow the prompts to play the game!

* How to run tests
To run tests, build the test project (making sure to install nuget packages) and run (from the root of the git repository):
./packages/NUnit.ConsoleRunner.3.6.1/tools/nunit3-console ./ConnectFourTests/bin/Debug/ConnectFourTests.dll

The test results should be printed to console and also be output to an xml file in the same directory as where you ran this command.


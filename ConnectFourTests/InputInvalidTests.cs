﻿using ConnectFour.Game;
using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using NUnit.Framework;
using System;

namespace ConnectFourTests
{
    [TestFixture]
    public class InputInvalidTests
    {
        [Test]
        public void MoveInvalidateNullPlayers()
        {
            Assert.Throws<ArgumentNullException>(delegate() {
                new Move(null, 1);
            });
        }

        [Test]
        public void MoveInvalidateIndexOutOfBounds()
        {
            Assert.Throws<ArgumentOutOfRangeException>(delegate () {
                new Move(new ConsolePlayer('O'), 50);
            });
            Assert.Throws<ArgumentOutOfRangeException>(delegate () {
                new Move(new ConsolePlayer('X'), -1);
            });
        }

        [Test]
        public void BoardInvalidateColumnIndexOutOfBounds()
        {
            Assert.Throws<IndexOutOfRangeException>(delegate () {
                var column = new Board()[7];
            });
            Assert.Throws<IndexOutOfRangeException>(delegate () {
                var column = new Board()[-7];
            });
        }

        [Test]
        public void BoardInvalidateOverflow()
        {
            Assert.Throws<OverflowException>(delegate ()
            {
                var board = new Board();
                var player = new ConsolePlayer('P');
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
                board.PlayMove(new Move(player, 0));
            });
        }
    }
}

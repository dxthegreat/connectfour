﻿using ConnectFour;
using ConnectFour.Game;
using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using NUnit.Framework;

namespace ConnectFourTests
{
    [TestFixture]
    public class GameWinCheckTests
    {
        ConnectFourGame game;
        IPlayer player1;
        IPlayer player2;
        [SetUp]
        public void SetUp()
        {
            player1 = new ConsolePlayer('O');
            player2 = new ConsolePlayer('X');
            game = new ConnectFourGame(player1, player2);
            game.board = new Board();
        }

        [Test]
        public void VerticalWin()
        {
            game.board.PlayMove(new Move(player1, 3));
            game.board.PlayMove(new Move(player1, 3));
            game.board.PlayMove(new Move(player1, 3));
            game.board.PlayMove(new Move(player1, 3));
            Assert.IsTrue(game.GameIsWonFor(player1));
            Assert.IsTrue(game.GameIsOver());
        }

        [Test]
        public void HorizontalWin()
        {
            game.board.PlayMove(new Move(player1, 3));
            game.board.PlayMove(new Move(player1, 4));
            game.board.PlayMove(new Move(player1, 5));
            game.board.PlayMove(new Move(player1, 6));
            Assert.IsTrue(game.GameIsWonFor(player1));
            Assert.IsTrue(game.GameIsOver());
        }

        [Test]
        public void DiagonalWin()
        {
            game.board.PlayMove(new Move(player1, 3));
            game.board.PlayMove(new Move(player2, 4));
            game.board.PlayMove(new Move(player1, 4));
            game.board.PlayMove(new Move(player2, 5));
            game.board.PlayMove(new Move(player2, 5));
            game.board.PlayMove(new Move(player1, 5));
            game.board.PlayMove(new Move(player2, 6));
            game.board.PlayMove(new Move(player2, 6));
            game.board.PlayMove(new Move(player2, 6));
            game.board.PlayMove(new Move(player1, 6));
            Assert.IsTrue(game.GameIsWonFor(player1));
            Assert.IsTrue(game.GameIsOver());
        }

        [Test]
        public void BackwardDiagonalWin()
        {
            game.board.PlayMove(new Move(player1, 6));
            game.board.PlayMove(new Move(player2, 5));
            game.board.PlayMove(new Move(player1, 5));
            game.board.PlayMove(new Move(player2, 4));
            game.board.PlayMove(new Move(player2, 4));
            game.board.PlayMove(new Move(player1, 4));
            game.board.PlayMove(new Move(player2, 3));
            game.board.PlayMove(new Move(player2, 3));
            game.board.PlayMove(new Move(player2, 3));
            game.board.PlayMove(new Move(player1, 3));
            Assert.IsTrue(game.GameIsWonFor(player1));
            Assert.IsTrue(game.GameIsOver());
        }
    }
}

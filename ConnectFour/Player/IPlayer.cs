﻿using ConnectFour.Game;

namespace ConnectFour.Player
{
    public interface IPlayer
    {
        char Token { get; }

        Move GetMove(ConnectFourGame game);
    }
}

﻿using ConnectFour.Game;
using ConnectFour.Game.Fixtures;
using System;
using System.Collections.Generic;

namespace ConnectFour.Player
{
    public class AIPlayer : IPlayer
    {
        private char _token;
        public char Token
        {
            get { return _token; }
            private set { _token = value; }
        }

        public AIPlayer(char Token)
        {
            _token = Token;
        }

        public Move GetMove(ConnectFourGame game)
        {
            var tried = new List<int>();

            while (tried.Count < Board.NUM_COLUMNS)
            {
                var moveCol = new Random().Next(0, Board.NUM_COLUMNS - 1);
                if (tried.Contains(moveCol))
                    continue;

                tried.Add(moveCol);
                var move = new Move(this, moveCol);
                if (game.CheckValid(move))
                    return move;
            }

            throw new Exception("Game is already Over!");
        }
    }
}

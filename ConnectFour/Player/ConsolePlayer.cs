﻿using ConnectFour.Game;
using System;

namespace ConnectFour.Player
{
    public class ConsolePlayer : IPlayer
    {
        private char _token;
        public char Token {
            get { return _token; }
            private set { _token = value; }
        }

        public ConsolePlayer(char Token)
        {
            _token = Token;
        }

        public Move GetMove(ConnectFourGame game)
        {
            game.PrintBoard();
            Console.Write("Select your next move! (type column number between 1 and 7): ");
            var colNumber = Console.ReadKey().KeyChar;
            while (colNumber < '1' || colNumber > '7' || !game.CheckValid(new Move(this, colNumber - '1')))
            {
                Console.WriteLine();
                game.PrintBoard();
                Console.Write("Invalid move! Please try again! (type column number between 1 and 7): ");

                colNumber = Console.ReadKey().KeyChar;
            }

            return new Move(this, colNumber - '1');
        }
    }
}

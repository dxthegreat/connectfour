﻿using ConnectFour;
using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using System;

class Program
{
    static void Main(string[] args)
    {
        var Game = new ConnectFourGame(new ConsolePlayer('X'), new AIPlayer('O'));
        while (true)
        {
            Game.Play(new Board());
            Console.WriteLine("Play Again? (Y/N)");
            var key = Console.ReadKey().KeyChar;
            while (key != 'Y' && key != 'y' && key != 'N' && key != 'n')
            {
                key = Console.ReadKey().KeyChar;
            }
            if (key == 'N' || key == 'n')
                return;
        }
    }
}
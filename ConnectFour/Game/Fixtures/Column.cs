﻿using System;
using System.Collections.Generic;

namespace ConnectFour.Game.Fixtures
{
    public class Column : List<char>
    {
        public static readonly int NUM_ROWS = 6;
        public Column() : base(NUM_ROWS)
        {
        }

        public char this[int columnIndex]
        {
            get
            {
                if (columnIndex >= NUM_ROWS)
                    throw new IndexOutOfRangeException();

                if (columnIndex >= Count)
                    return ' ';
                
                return base[columnIndex];
            }
            private set
            {
                this[columnIndex] = value;
            }
        }

        public void PlayMove(Move move)
        {
            if (Count >= NUM_ROWS)
                throw new OverflowException("Too many pieces has been placed in this column!");
            Add(move.Token);
        }
    }
}

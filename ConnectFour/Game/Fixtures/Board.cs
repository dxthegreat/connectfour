﻿using System;
using System.Collections.Generic;

namespace ConnectFour.Game.Fixtures
{
    public class Board
    {
        public static readonly int NUM_COLUMNS = 7;

        private IList<Column> columns;

        public Board()
        {
            columns = new Column[NUM_COLUMNS];
            for (var i = 0; i < NUM_COLUMNS; i++)
            {
                columns[i] = new Column();
            }
        }

        public Column this[int columnIndex] {
            get
            {
                if (columnIndex >= NUM_COLUMNS || columnIndex < 0)
                    throw new IndexOutOfRangeException($"Column index must be between 0 and { NUM_COLUMNS - 1 }");
                        
                return columns[columnIndex];
            }
            private set
            {
                columns[columnIndex] = value;
            }
        }

        public void PlayMove(Move move)
        {
            this[move.ColNumber].PlayMove(move);
        }
    }
}

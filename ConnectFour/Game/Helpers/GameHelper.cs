﻿using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using System.Text;

namespace ConnectFour.Game
{
    public static class GameHelper
    {
        public static readonly int NUM_TO_WIN = 4;

        public static bool GameIsOver(this ConnectFourGame game)
        {
            if (game.GameIsWonFor(game.firstPlayer) || game.GameIsWonFor(game.lastPlayer))
                return true;

            for (int i = 0; i < Board.NUM_COLUMNS; i++)
            {
                if (game.board[i].Count <= Column.NUM_ROWS)
                    return false;
            }
            return true;
        }

        public static bool GameIsWonFor(this ConnectFourGame game, IPlayer player)
        {
            var token = player.Token;
            var sb = new StringBuilder();
            for (var i = 0; i < NUM_TO_WIN; i++)
            {
                sb.Append(token);
            }
            var stringToWin = sb.ToString();

            //check verticals
            for (var i = 0; i < Board.NUM_COLUMNS; i++)
            {
                var colChars = "";
                for (var j = 0; j < Column.NUM_ROWS; j++)
                {
                    colChars += game.board[i][j];
                }
                if (colChars.Contains(stringToWin))
                    return true;
            }

            //check rows
            for (var i = 0; i < Column.NUM_ROWS; i++)
            {
                var rowChars = "";
                for (var j = 0; j < Board.NUM_COLUMNS; j++)
                {
                    rowChars += game.board[j][i];
                }
                if (rowChars.Contains(stringToWin))
                    return true;
            }

            //check forward diagonals
            for (var i = -Board.NUM_COLUMNS; i < Board.NUM_COLUMNS; i++)
            {
                var forwardChars = "";
                for (var j = 0; j < Column.NUM_ROWS; j++)
                {
                    if (j + i >= Board.NUM_COLUMNS || j + i < 0)
                        forwardChars += ' ';
                    else
                        forwardChars += game.board[i+j][j];
                }
                if (forwardChars.Contains(stringToWin))
                    return true;
            }

            //check backward diagonals
            for (var i = 0; i < 2 * Board.NUM_COLUMNS; i++)
            {
                var backwardChars = "";
                for (var j = 0; j < Column.NUM_ROWS; j++)
                {
                    if (i - j >= Board.NUM_COLUMNS || i - j < 0)
                        backwardChars += ' ';
                    else
                        backwardChars += game.board[i-j][j];
                }
                if (backwardChars.Contains(stringToWin))
                    return true;
            }

            return false;
        }
    }
}

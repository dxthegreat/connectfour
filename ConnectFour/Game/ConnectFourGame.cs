﻿using ConnectFour.Game;
using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using System;

namespace ConnectFour
{
    public class ConnectFourGame
    {
        public IPlayer firstPlayer;
        public IPlayer lastPlayer;
        public IPlayer currentPlayer;
        public Board board;

        public ConnectFourGame(IPlayer firstPlayer, IPlayer lastPlayer)
        {
            this.firstPlayer = firstPlayer;
            this.lastPlayer = lastPlayer;
        }

        public void Play(Board board)
        {
            this.board = board;
            currentPlayer = firstPlayer;
            PrintBoard();

            while (!this.GameIsOver())
            {
                Move nextMove = currentPlayer.GetMove(this);
                while (!CheckValid(nextMove))
                {
                    Console.WriteLine("Move is not valid");
                    nextMove = currentPlayer.GetMove(this);
                }
                
                board.PlayMove(nextMove);
                
                currentPlayer = currentPlayer == firstPlayer ? lastPlayer : firstPlayer; 
            }

            PrintBoard();
            Console.WriteLine("Game is Over!");
            EvaluateGame();
        }

        public bool CheckValid(Move move)
        {
            if (move == null)
                return false;

            if (board[move.ColNumber].Count >= Column.NUM_ROWS)
                return false;
            
            return true;
        }

        public void PrintBoard()
        {
            Console.Clear();
            for (var rowNum = 0; rowNum < Column.NUM_ROWS * 2 + 1; rowNum++) {
                for (var colNum = 0; colNum < Board.NUM_COLUMNS * 2 + 1; colNum++)
                {
                    if (rowNum % 2 == 0)
                        Console.Write('-');
                    else if (colNum % 2 == 0)
                        Console.Write('|');
                    else
                    {
                        var maxRowIndex = Column.NUM_ROWS - 1;

                        Console.Write(board[colNum / 2][maxRowIndex - rowNum / 2]);
                    }
                }
                Console.WriteLine();
            }
            Console.Write('|');
            for (var i = 0; i < Board.NUM_COLUMNS; i++)
            {
                if (CheckValid(new Move(currentPlayer, i)))
                    Console.Write((i + 1) + "|");
                else
                    Console.Write(" |");
            }
            Console.WriteLine("");
        }
        
        private void EvaluateGame()
        {
            if (this.GameIsWonFor(firstPlayer))
            {
                Console.WriteLine("Player one wins!");
            }
            else if (this.GameIsWonFor(lastPlayer))
            {
                Console.WriteLine("Player two wins!");
            }
            else
            {
                Console.WriteLine("Nobody Wins...");
            }
        }
    }
}

﻿using ConnectFour.Game.Fixtures;
using ConnectFour.Player;
using System;

namespace ConnectFour.Game
{
    public class Move
    {
        private int _colNumber;
        public int ColNumber {
            get { return _colNumber; }
            private set { _colNumber = value; }
        }
        public int Index {
            get { return _colNumber - 1; }
        }

        private char _token;
        public char Token {
            get { return _token; }
            private set { _token = value; }
        }


        public Move(IPlayer madeBy, int colNumber)
        {
            if (madeBy == null)
            {
                throw new ArgumentNullException("Player not specified");
            }

            if (colNumber >= Board.NUM_COLUMNS || colNumber < 0)
            {
                throw new ArgumentOutOfRangeException("Column number provided is out of bounds");
            }
            _colNumber = colNumber;
            _token = madeBy.Token;
        }

        public Move(IPlayer madeBy, string colNumber) : this(madeBy, int.Parse(colNumber))
        {
        }
    }
}
